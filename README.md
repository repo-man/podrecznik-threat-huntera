# Podręcznik Threat Huntera

Podręcznik Łowcy Zagrożeń

![top.png](./top.png)



[TOC]

## Narzędzia

**base64** - enkodowanie i dekodowanie (`-d`) algorytmem base64.

**binwalk** - pozwala na analizę pliku pod kątem wystepujących sygnatur (por. Magic Bytes), ciągów znaków, zmian entropi (stopień losowości), różnic względem innego pliku.
```bash
> binwalk wall.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 1920 x 1080, 8-bit/color RGB, non-interlaced
62            0x3E            Zlib compressed data, default compression
```

**curl** - client ftp, http, https itp. Pozwala w prosty sposób zobaczyć źródło strony lub zapisać plik. Ponieważ serwery potrafią reagować dynamicznie zależnie od parametrów rządania, przydaja się najczęsciej parametry `-A` (User Agent) oraz `-e` (Referer). Flaga `-k` pozwala ignorować błedy połączeń wynikające z niezaufanych certyfikatów. Parametr `-x` przydaje się gdy musimy przejść przez serwer proxy, parametr `-X` pozwala nam wybrac metodę którą chcemy zastosować, `-L` by podążyć za nagłówkiem Location.
```bash
> curl https://wp.pl -LI
HTTP/2 301 
server: nginx
date: Thu, 14 Mar 2019 23:04:33 GMT
content-type: text/html
content-length: 178
location: https://www.wp.pl/

HTTP/2 200 
server: nginx
date: Thu, 14 Mar 2019 23:04:33 GMT
content-type: text/html; charset=utf-8
content-length: 0
```


**hexdump** - pozwala na zamianę danych wejściowych na kod szesnastkowy.
```bash
> hexdump -n 32 -C wall.jpg
00000000  89 50 4e 47 0d 0a 1a 0a  00 00 00 0d 49 48 44 52  |.PNG........IHDR|
00000010  00 00 07 80 00 00 04 38  08 02 00 00 00 67 b1 56  |.......8.....g.V|
```

**sslyze** - przydatne narzędzie do analizy warstwy SSL, certyfikatów oraz ustawień serwera obsługującego połaczenia SSL/HTTPS.

**tshark** - przechwytuje i analizuje ruch sieciowy. Instalowany wraz z narzędziem Wireshark.

**tcpdump** - przechwytuje i analizuje ruch sieciowy.

**xxd** - pozwala na zamianę danych wejściowych na kod szesnastkowy i z powrotem. Z flagą `-b` na kod binarny. [Przykład](#d-hash).



## miniSłownik

**Artefakt** - ślad pozostawony przez intruza lub malware. Np charakterystyczne zdarzenia (event Id) w EventLogu lub modyfikacje wybranych gałęzi rejestru, czy też usunięcie zawartości `bash_history`.

**Hamminga odległość** - ilość zmian/akcji potrzebnych by jeden ciąg znaków zamienić w drugi.
```bash
> ./hamming.sh 86f7e437faa5a7fce15d1ddcb9eaeaea377667b8 84a516841ba77a5b4648de2cd0dfcb30ea46dbb4
35
> ./hamming.sh a z
1
```

**Hash** - <a name="d-hash"></a> rezultat działania fukcji hashującej zwanej również funkcją skrótu. Celem jest obliczenie krótkiego i unikalnego ciągu znaków charakterystycznego dla danych wejściowych. Ponieważ na ogół mamy do czynienia ze znaczącą redukcją rozmiaru między danymi wyjściowymi i wyjściowymi niemożliwym jest odwrócenie funkcji by na podstawie hasha uzyskać dane wejściowe. Idealna funkcja skrótu jest wolna od kolizji - nigdy nie zwróci tego samego ciagu znaków dla różnych danych wejściowych. Właśnie ze względu na wykrycie kolizji w niektórych funkcjach, uznaje się je za niebezpieczne i powoli wychodzą z użycia (md5, sha1). Warto wspomnieć, że zmiana jednego bitu w danych wejściowych wygeneruje drastycznie inny hash.
```bash
> echo -n 'a' | xxd -b
00000000: 01100001
> echo -n 'c' | xxd -b
00000000: 01100011

> echo -n 'a' | shasum
86f7e437faa5a7fce15d1ddcb9eaeaea377667b8  -
> echo -n 'c' | shasum
84a516841ba77a5b4648de2cd0dfcb30ea46dbb4  -
```

**IOC** - Indicator Of Compromise. Ślad dowodzący zaistnieniu próby infekcji lub infiltracji: domena, URL, nazwa pliku, Hash, adres e-mail, adres IP, User Agent, charakterystyczny ciąg znaków, zestaw instrukcji kodu maszynowego.

**Magic bytes** - pocztąkowe Bajty w pliku pozwalają na identyfikację formatu tego pliku.

**Malware** - złośliwe oprogramowanie. Często dzielone na kategorie ze względu na charakter działań wykorzystywanych przez program, lub ze względu na intencje autora programu. Ransomware, Spyware, Adware, Scamware, Trojany, Robaki, Wirusy, RAT itp.

**PAP** - Permitable Action Protocol. Zgodnie z nazwą określa zestaw akcji jakie analityk może podjać w kontekście danego artefaktu/IOC.

**Shell code** - specjalnie przygotowany zestaw instrukcji, który po przekazaniu do atakowanego systemu pozwoli nam na obejście lub przełamanie istniejących zabezpieczeń. Często pozwala na eskalację uprawnień użytkownika, dostęp do powłoki, obejście ograniczeń narzuconych przez system czy wykonanie połączenia na wybrany adres IP.

**TLP** - Traffic Light Protocol. System klasyfikacji artefaktów/IOC na takie które mogą być udostępniane poza organizacją, takie do których dostęp powinna mieć tylko wąska grupa analityków oraz pośrednie. (Na przyklad Green, Amber, Red)





